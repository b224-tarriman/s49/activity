// fetch() method in JavaScript is used to send request in the server and load the received response in the webpage/s. The request and response is in JSON format.

/* 
    Syntax:
        fetch("url", {option})
            url - this is the link address which the request is to be made and source where the response will come from.(endpoint)
            options/s - array or properties that contains the HTTP method, body of request and headers.    

*/

// GET post data / Retrieve Function
/* fetch("https://jsonplaceholder.typicode.com/posts")
.then((res)=>res.json())
.then((data)=>console.log(data)) */

fetch("https://jsonplaceholder.typicode.com/posts")
.then((res)=>res.json())
.then((data)=>showPosts(data))

document.querySelector("#form-add-post").addEventListener("submit", (e)=>{
    e.preventDefault()
    fetch("https://jsonplaceholder.typicode.com/posts",{
        method:"POST",
        body: JSON.stringify({
            title: document.querySelector("#txt-title").value,
            body: document.querySelector("#txt-body").value,
            userId: 1
        }),
        headers:{
            "Content-Type":"application/json"
        }
    })
    .then((res)=> res.json())
    .then((data)=>{
        console.log(data)
        alert("Post Successfully Added!")
    })

    title: document.querySelector("#txt-title").value = null
    body: document.querySelector("#txt-body").value = null
});


// VIEW POST
const showPosts = (posts) => {
	//Create a varaible that will contain all the posts.
	let postEntries = "";


	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});

	// To check what is stored in the postEntries variables.
	// console.log(postEntries)

	// To assign the value of postEntries to the element with "div-post-entries" id.
	document.querySelector("#div-post-entries").innerHTML = postEntries

};

const deletePost = (id) =>{
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
        method:"DELETE"
    })
    .then((res)=>res.json())
    .then(()=>{
        fetch(`https://jsonplaceholder.typicode.com/posts/${id}`).then((res)=>res.json()).then((result)=>{
            
            console.log(`Deleted ID #: ${result.id}`)
            console.log(result)
            document.getElementById(`post-${id}`).remove()
            alert(`POST Succesfully Deleted`)
        })
    })
}

// EDIT POST DATA / Edit Button

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

    document.querySelector("#btn-submit-update").removeAttribute("disabled")
};

// UPDATE POST

document.querySelector("#form-edit-post").addEventListener("submit",(e)=>{
    e.preventDefault()
    let id = document.querySelector("#txt-edit-id").value

    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
        method:"PUT",
        body: JSON.stringify({
            id: id,
            title: document.querySelector("#txt-edit-title").value, 
            body: document.querySelector("#txt-edit-body").value,
            userId:1
        }),
        headers:{
            "Content-Type":"application/json"
        }
    })
    .then((res)=>res.json())
    .then((data)=>{
        console.log(data)
        alert("POST Successfully updated!")
    })

    document.querySelector("#txt-edit-title").value = null
    document.querySelector("#txt-edit-body").value = null

    document.querySelector("#btn-submit-update").setAttribute("disabled",true)
});